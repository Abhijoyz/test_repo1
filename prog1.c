#include<stdio.h>
#include"mymath.h"
int main()
{
	double x , y;
	printf("Enter first number\n");
	scanf("%lf",&x);
	printf("Enter second number\n");
	scanf("%lf",&y);
	double ans1 = my_add(x,y);
	double ans2 = my_sub(x,y);
	double ans3 = my_mul(x,y);
	printf("a+b = %7.2lf\n",ans1);
	printf("a-b = %7.2lf\n",ans2);
	printf("a*b = %7.2lf\n",ans3);
	return 0;
}
